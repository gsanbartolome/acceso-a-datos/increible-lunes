<?php

use yii\helpers\Html;

$this->title= 'Consultas de Seleccion 1';
?>

<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Consultas de Seleccion</h1>
    </div>
        
    <div class="body-content">

        <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                <h3>Consulta 1</h3>

                <p>Listar las edades de los ciclistas (Sin repetidos)</p>

                <p> 
                    <?= Html::a('Active Record', ['site/consultala'], ['class'=>'btn btn-primary'])?>
                    <?= Html::a('DAO', ['site/consultala'], ['class'=>'btn btn-warning'])?>
                
                </p>
                    </div>
                </div>
            </div>
        </div>

    </div>